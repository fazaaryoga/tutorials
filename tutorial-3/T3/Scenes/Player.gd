extends KinematicBody2D


export (int) var speed = 400
export (int) var boostSpeed = 600
export (int) var jump_speed = -600
export (int) var gravity = 1200

const UP = Vector2(0, -1)

var velocity = Vector2()
var jumpCount = 0

func get_input(delta):
	velocity.x = 0
	if Input.is_action_just_pressed("up") and jumpCount < 1:
		jumpCount += 1
		velocity.y = jump_speed
	if Input.is_action_pressed("right"):
			velocity.x = speed
	if Input.is_action_pressed("left"):
		velocity.x = -speed
	if (Input.is_action_pressed("right") or Input.is_action_pressed("left")) and Input.is_action_pressed("boost"):
		if velocity.x > 0:
			velocity.x = boostSpeed
		else:
			velocity.x = -boostSpeed
	
		
func _physics_process(delta):
	velocity.y += delta * gravity
	get_input(delta)
	jumpCount = 0  if is_on_floor() else jumpCount
	if velocity.x != 0 or !is_on_floor():
		if  !is_on_floor():
			if velocity.y > 0:
				$AnimatedSprite.animation = "fall"	
			elif velocity.y < 0:
				$AnimatedSprite.animation = "jump"	
		else:
			$AnimatedSprite.animation = "walk"
		$AnimatedSprite.flip_h = velocity.x < 0
		$AnimatedSprite.play()
	else: 
		$AnimatedSprite.animation = "walk"
		$AnimatedSprite.stop()
		
	velocity = move_and_slide(velocity, UP)

